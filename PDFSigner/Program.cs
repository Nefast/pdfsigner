﻿/*
 *  ====================================================================
 *              LICENCE PLEASE READ BEFORE ANY MODIFICATIONS
 *  ====================================================================
 *  PDFSigner is a program made for CNIM Design office with IText librairie.
 *  Developed for add a digital signatures on a pdf drawing without use the
 *  professional version of Adobe Acrobat.
 *  Copyright (C) 2014 - Nefast < https://bitbucket.org/Nefast >
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *   
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *  TODO-LIST v0.4:
 *  [x] first push on BitBucket
 *  [x] fix to long name for IsMatch()
 *  [x] add elapsed time
 *  [x] can pass file if have not revision (enter 'P' for revision)
 *  [x] console clean : remove debugs output
 *  v0.3:
 *  [x] fix sign rotation bug
 *  [x] add revision offset support
 *  [x] add config file for offsets setting
 *  v0.2 :
 *  [x] stamping at the good coordinates for any size of pdf
 *  [x] stamping at the good coordinates for any rotations
 *  [x] auto-rotate to landscape
 *  v0.1:
 *  [x] load pdf file from args[] (drag & drop launch)
 *  [x] add three stamp at the good coordinates (for now only A1H is supported)
 *  [x] save into a new file at the same directory
 * 
 *  General TODO-LIST :
 *  [ ] add already signed pdf detection
 *  [x] fix to long name for IsMatch()
 *  [x] add elapsed time
 *  [x] can pass file if have not revision
 *  [x] console clean
 *  [ ] code refactoring (create some class)
 *  [ ] add small cartridge support
 *  [ ] add vertical pdf support
 *  [ ] add graphical interface
 *  [x] fix sign rotation bug (from 0.2)
 *  [x] add revision offset support
 *  [x] add config file for offsets setting
 *  [x] stamping at the good coordinates for any size of pdf
 *  [x] stamping at the good coordinates for any rotations
 *  [x] load pdf file from args[] (drag & drop launch)
 *  [x] add three stamp at the good coordinates
 *  [x] save into a new file at the same directory
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using System.Configuration;
using System.Text.RegularExpressions;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.text.pdf.parser;
using System.util.collections;
using iTextSharp.text;
using iTextSharp.text.pdf.security;
using iTextSharp.text.pdf.crypto;

namespace PDFSigner
{
	class MainClass
	{
		public static void processFile(string path)
		{
			try
			{
				string fileName = path.Split('\\').GetValue(path.Split('\\').Length-1).ToString();
				string shortPath;
				if(fileName.Length < 30)
				{
					shortPath = fileName;
				}
				else
				{
					shortPath = fileName.Remove(30);
				}
				string outPath = path.Replace(fileName,"Stamped/"+fileName);
				Console.WriteLine ("[LOG] : Process {0}", fileName);
				if (!fileName.Contains (".pdf")) 
				{
					Console.WriteLine ("[ERR] : This file is not a pdf file.");
					return;
				}
				if(!Directory.Exists("Stamped"))
				{
					Directory.CreateDirectory("Stamped");
				}
				// -- found revision
				String revRegex = ConfigurationManager.AppSettings["RevisionRegexSearch"].ToString();
				Regex revisionInfosContents = new Regex(revRegex);
				int revision = 0;
				if(revisionInfosContents.IsMatch(shortPath))
				{
					string match = revisionInfosContents.Match(shortPath).ToString();
					revisionInfosContents = new Regex("\\d{1,}");
					revision = Convert.ToInt32(revisionInfosContents.Match(match).ToString());
				}
				else
				{
					Console.Write("[LOG] : Please Specify the revision : ");
					String usrCmd = Console.ReadLine();
					if(usrCmd.Contains("p"))
					{
						Console.WriteLine ("[LOG] : {0} aborded by user!",fileName);
						return;
					}
					revision = Convert.ToInt32(usrCmd);
				}
				//Console.WriteLine("[DBG] : Document revision : {0}",revision);
				if(revision > 4) // for prevent misplaced forms
				{
					revision = 4;
				}
				// -- end
				//Console.WriteLine ("[DBG] : Path length = {0}",path.Length);
				PdfReader reader = new PdfReader(path);
				Rectangle pageRec = reader.GetPageSizeWithRotation(1);
				FileStream outFile = new FileStream(outPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
				PdfStamper stamp = new PdfStamper(reader, outFile);
				stamp.RotateContents = false;
				float y = pageRec.GetTop(0);
				float x = pageRec.GetRight(0);
				if(x < y) // if is vertical page we rotate them 
				{
					PdfDictionary pageDict = reader.GetPageN(1);
					pageDict.Put(PdfName.ROTATE, new PdfNumber(270));
					//Console.WriteLine("[DBG] : Rotate page to landscape.");
					float tmpX = y;
					y = x;
					x = tmpX;
				}
				float stampOffset_X = x - Convert.ToSingle(ConfigurationManager.AppSettings["StampOffset_X"]);
				float stampOffset_Y = Convert.ToSingle(ConfigurationManager.AppSettings["StampOffset_Y"]);
				float stampSize_X = Convert.ToSingle(ConfigurationManager.AppSettings["StampSize_X"]);
				float stampSize_Y = Convert.ToSingle(ConfigurationManager.AppSettings["StampSize_Y"]);
				float revisionTextSize = Convert.ToSingle(ConfigurationManager.AppSettings["RevisionTextSize"]);
				float revisionHeight = Convert.ToSingle(ConfigurationManager.AppSettings["RevisionHeight"]);
				float baseRevision = Convert.ToSingle(ConfigurationManager.AppSettings["BaseRevision"]);
				float revisionCase = revision - baseRevision;
				float nextStampOffset = stampOffset_X;
				stampOffset_Y += revisionHeight * revisionCase;
				//Console.WriteLine("[DBG] : Document size : {0},{1}.",x,y);
				for(int i = 0; i < 3;i++)
				{
					 
					PdfFormField signField = PdfFormField.CreateSignature(stamp.Writer);
					signField.FieldName = "signature"+i.ToString();
					signField.SetWidget(new Rectangle(nextStampOffset, stampOffset_Y, nextStampOffset+stampSize_X, stampOffset_Y+stampSize_Y), PdfAnnotation.HIGHLIGHT_OUTLINE);
					signField.Flags = PdfAnnotation.FLAGS_PRINT;
					signField.MKRotation = 270; // FIX for sign rotation bug
					stamp.AddAnnotation(signField,1);

					if(i == 0)
					{
						nextStampOffset += revisionTextSize;
					}
					else if(i == 1)
					{
						nextStampOffset += stampSize_X;
					}
					else
					{
						nextStampOffset = stampOffset_X;
					}
				}
				stamp.Close();
				reader.Close();
				outFile.Close ();
				Console.WriteLine ("[LOG] : {0} stamped succefuly !",fileName);
			}
			catch (Exception e)
			{
				Console.WriteLine ("[ERR] : Unhandled exception : {0}",e.Message);
			}

		}

		public static void Main (string[] args)
		{
			Stopwatch watch = new Stopwatch();
			watch.Start ();
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine ("***************************************");
			Console.WriteLine ("****  Welcome to PDFSigner - V0.4  ****");
			Console.WriteLine ("***************************************");
			int i = 0;
			foreach (string arg in args) 
			{
				processFile (arg);
				i++;
			}
			watch.Stop ();
			if (i > 0) 
			{
				Console.WriteLine ("[LOG] : {0} pdf stamped in : {1} sec", i, watch.ElapsedMilliseconds / 1000f);
			}

			Console.WriteLine ("Press any key to continue ...");
			Console.ReadKey ();
		}
	}
}